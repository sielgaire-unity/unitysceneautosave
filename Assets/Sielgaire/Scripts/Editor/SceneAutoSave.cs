using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

/// <summary>
/// This class adds a an autosave feature to save the currently
/// worked on scene either every 5 minutes or when it is played.
///
/// Shawn Newsom
/// </summary>

namespace Sielgaire.Editor.AutoSave
{
    [InitializeOnLoad]
    public class SceneAutoSave : EditorWindow
    {
        private static string SOPPrefKey = "SaveOnPlay";
        private static string SATPrefKey = "AutoTimeSave";
        private static string STPrefKey = "SaveTime";
        private static string TBSPrefKey = "TimeBetweenSaves";
        private static float defaultSaveTime = 300;

        static string saveFolder = "AutoSave.tmp";
        static string extension = ".unity";

        //#region MenuToBeRemovedLater
        //[MenuItem("Test/Save on Play On")]
        //private static void TurnSaveOnPlayOn()
        //{
        //    saveOnPlay = true;
        //}
        //[MenuItem("Test/Save on Play Off")]
        //private static void TurnAutoTimeOff()
        //{
        //    saveOnPlay = false;
        //}

        //[MenuItem("Test/Save on ATPlay On")]
        //private static void TurnAutoTImeSaveOn()
        //{
        //    saveAutoTime = true;
        //}
        //[MenuItem("Test/Save on ATPlay Off")]
        //private static void TurnSaveOnPlayOff()
        //{
        //    saveAutoTime = false;
        //}
        //#endregion

        #region AutoTimeSave
        /// <summary>
        /// Turns on save every specific time period.
        /// True is On
        /// False is off
        /// </summary>
        public static bool saveAutoTime
        {
            get
            {
                if (!EditorPrefs.HasKey(SATPrefKey))
                    EditorPrefs.SetBool(SATPrefKey, false);
                return EditorPrefs.GetBool(SATPrefKey);
            }
            set
            {
                if (value)
                {
                    EditorPrefs.SetBool(SATPrefKey, true);
                    //EditorApplication.update -= OnUpdate;
                    EditorApplication.update += OnUpdate;
                    saveTime = GetNewSaveTime();
                }
                else
                {
                    EditorPrefs.SetBool(SATPrefKey, false);
                    EditorApplication.update -= OnUpdate;
                }
            }
        }

        /// <summary>
        /// The time that the next auto time save happens
        /// </summary>
        public static float saveTime
        {
            get
            {
                if (!EditorPrefs.HasKey(STPrefKey))
                {
                    float tempNextSaveTime = GetNewSaveTime();
                    EditorPrefs.SetFloat(STPrefKey, tempNextSaveTime);
                }
                return EditorPrefs.GetFloat(STPrefKey);
            }
            set
            {
                EditorPrefs.SetFloat(STPrefKey, value);
            }
        }

        /// <summary>
        /// The time between saves.
        /// This is here because I plan on making the time changable through the window
        /// </summary>
        public static float timeBetweenSaves
        {
            get
            {
                if (!EditorPrefs.HasKey(TBSPrefKey))
                    EditorPrefs.SetFloat(TBSPrefKey, defaultSaveTime);
                return EditorPrefs.GetFloat(TBSPrefKey);
            }
            set
            {
                EditorPrefs.SetFloat(TBSPrefKey, value);
            }
        }

        static void OnUpdate()
        {
            if (!Application.isPlaying && EditorApplication.timeSinceStartup > saveTime)
            {
                saveTime = GetNewSaveTime();
                SaveScene();
            }
        }

        private static float GetNewSaveTime()
        {
            return (float)EditorApplication.timeSinceStartup + timeBetweenSaves;
        }
        #endregion

        #region SaveOnPlay
        /// <summary>
        /// Turns Save on Play on and off
        /// True = on
        /// False = off
        /// </summary>
        public static bool saveOnPlay
        {
            get
            {
                if (EditorPrefs.HasKey(SOPPrefKey))
                    return EditorPrefs.GetBool(SOPPrefKey);
                return false;
            }
            set
            {
                if (value)
                    EditorApplication.playModeStateChanged += OnPlayModeChanged;
                else
                    EditorApplication.playModeStateChanged -= OnPlayModeChanged;
                EditorPrefs.SetBool(SOPPrefKey, value);
            }
        }

        private static void OnPlayModeChanged(PlayModeStateChange state)
        {
            if (state == PlayModeStateChange.ExitingEditMode)
                SaveScene();
        }
        #endregion

        #region SaveScene
        static void SaveScene()
        {
            Scene scene = SceneManager.GetActiveScene();
            string backupName = Application.dataPath + "/" + saveFolder + "/" + scene.name.ToString().Replace(" ", "") + MDYToString() + HMSToString() + EditorApplication.timeSinceStartup.ToString() + extension;
            string autosaveFolder = Application.dataPath + "/" + saveFolder + "/";

            if (!System.IO.Directory.Exists(autosaveFolder))
            {
                System.IO.Directory.CreateDirectory(Application.dataPath + "/" + saveFolder);
                Debug.Log("AutoSave: Save Folder Didn't Exist: " + Application.dataPath + saveFolder);
                AssetDatabase.Refresh();
            }

            bool saveOK = EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene(), backupName, true);
            if (!saveOK) Debug.Log("AutoSave: Save Scene ERROR! " + backupName);
            AssetDatabase.Refresh();
        }

        static string MDYToString()
        {
            return "" + System.DateTime.Now.Month + System.DateTime.Now.Day + System.DateTime.Now.Year;
        }

        static string HMSToString()
        {
            return "" + System.DateTime.Now.ToLongTimeString().Replace(":", "");
        }
        #endregion

        /// <summary>
        /// Delete the save prefs
        /// If there is a problem with saving, delete the prefs first...
        /// </summary>
        public static void DeleteSavePrefs()
        {
            if (EditorPrefs.HasKey(SOPPrefKey))
                EditorPrefs.DeleteKey(SOPPrefKey);
            if (EditorPrefs.HasKey(SATPrefKey))
                EditorPrefs.DeleteKey(SATPrefKey);
            if (EditorPrefs.HasKey(STPrefKey))
                EditorPrefs.DeleteKey(STPrefKey);
            if (EditorPrefs.HasKey(TBSPrefKey))
                EditorPrefs.DeleteKey(TBSPrefKey);
            EditorApplication.update -= OnUpdate;
            EditorApplication.playModeStateChanged -= OnPlayModeChanged;
        }

        #region Constructor
        static SceneAutoSave()
        {
            EditorApplication.playModeStateChanged -= OnPlayModeChanged;
            if (!EditorPrefs.HasKey(SOPPrefKey))
                EditorPrefs.SetBool(SOPPrefKey, false);
            if (saveOnPlay)
                EditorApplication.playModeStateChanged += OnPlayModeChanged;

            EditorApplication.update -= OnUpdate;
            if (!EditorPrefs.HasKey(SATPrefKey))
                EditorPrefs.SetBool(SATPrefKey, false);
            if (saveAutoTime)
                EditorApplication.update += OnUpdate;

            if (!EditorPrefs.HasKey(STPrefKey))
            {
                float tempNextSaveTime = GetNewSaveTime();
                EditorPrefs.SetFloat(STPrefKey, tempNextSaveTime);
            }
        }
        #endregion
    }
}