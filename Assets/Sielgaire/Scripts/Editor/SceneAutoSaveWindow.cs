using UnityEngine;
using UnityEditor;

/// <summary>
/// The code for the editor window...
/// I originally only wanted 1 file but because I used the editorprefs I needed to split it into two...
/// 
/// This window can be close when ever you change the save states (on play or timed).
/// If there is a wierd glitch just delete the prefs (editor scripting can be flakey sometimes)
/// 
/// Shawn Newsom
/// </summary>

namespace Sielgaire.Editor.AutoSave
{
    public class SceneAutoSaveWindow : EditorWindow
    {
        const string MenuAutoSave = "Sielgaire/Auto Save";

        [MenuItem(MenuAutoSave, false, 126)]
        public static void Init()
        {
            SceneAutoSaveWindow window = (SceneAutoSaveWindow)EditorWindow.GetWindow(typeof(SceneAutoSaveWindow), true, "AutoSave Panel");
            window.Show();
        }

        private void Status()
        {
            EditorGUILayout.LabelField("Compiling:", EditorApplication.isCompiling ? "Yes" : "No");
            EditorGUILayout.LabelField("Paused:", EditorApplication.isPaused ? "Yes" : "No");
            EditorGUILayout.LabelField("Playing:", EditorApplication.isPlaying ? "Yes" : "No");
            EditorGUILayout.LabelField("Playing or Will Play:", EditorApplication.isPlayingOrWillChangePlaymode ? "Yes" : "No");
            EditorGUILayout.LabelField("Updating:", EditorApplication.isUpdating ? "Yes" : "No");
            EditorGUILayout.LabelField("Time Since Startup:", EditorApplication.timeSinceStartup.ToString());
        }


        private static void BackupOnPlay()
        {
            string sop = "Backup on Play: OFF";
            if (SceneAutoSave.saveOnPlay)
                sop = "Backup On Play: On";

            if (GUILayout.Button(sop))
                SceneAutoSave.saveOnPlay = !SceneAutoSave.saveOnPlay;
        }

        void AutoBackup()
        {
            string ab = "AutoBackup: OFF";
            if (SceneAutoSave.saveAutoTime)
                ab = "AutoBackup: On";

            if (GUILayout.Button(ab))
                SceneAutoSave.saveAutoTime = !SceneAutoSave.saveAutoTime;

            if (SceneAutoSave.saveAutoTime)
                EditorGUILayout.LabelField("Next Save TIme: ", SceneAutoSave.saveTime.ToString());
        }

        void DeleteSavePrefs()
        {
            string dsp = "Delete Auto Save Prefs";
            if (GUILayout.Button(dsp))
                SceneAutoSave.DeleteSavePrefs();
        }

        void OnGUI()
        {
            Status();
            BackupOnPlay();
            AutoBackup();
            DeleteSavePrefs();
        }
    }
}